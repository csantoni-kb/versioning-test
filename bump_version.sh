if [[ -z $(git status -s) ]]
then
  echo "GIT tree is clean"
else
  echo -e "\e[31mERROR:\e[39m Please commit changes before running this"
  read -n 1 -s -r -p "Press any key to continue" 
  exit
fi

echo -e "\e[32m================ CURRENT VERSION ================\e[39m"
node -p "require('./package.json').version"

echo -e "\e[32m================ CHOOSE BUMP TYPE ================\e[39m"
read -p 'Choose the bum type (major/minor/patch): '
echo ""
echo "You typed ${REPLY}"

if [[ "${REPLY}" == "patch" ]] || [[ "${REPLY}" == "minor" ]] || [[ "${REPLY}" == "major" ]] 
then
    echo "Valid value"
else
    echo -e "\e[31mERROR:\e[39m The only available values are major, minor and patch"
    read -n 1 -s -r -p "Press any key to continue" 
    exit
fi

echo -e "\e[32m================ FETCHING BRANCHES ================\e[39m"
git checkout develop
git fetch --all

echo -e "\e[32m================ MERGE MASTER -> DEV ================\e[39m"
git merge master
git push origin develop

echo -e "\e[32m================ MERGE DEV -> MASTER ================\e[39m"
git checkout master
git merge develop
git push origin master

echo -e "\e[32m================ BUMPING VERSION ================\e[39m"
npm version ${REPLY}

echo -e "\e[32m================ PUSHING TO MASTER ================\e[39m"
git push origin master

echo -e "\e[32m================ UPDATING DEVELOP ================\e[39m"
git checkout develop
git merge master

echo -e "\e[32m================ VERSION UPDATED ================\e[39m"
read -n 1 -s -r -p "Press any key to continue" 